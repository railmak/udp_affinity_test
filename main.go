package main

import (
	"flag"
	"fmt"
	"net"
	"os"
	"regexp"
	"sync"
	"time"
)

type options struct {
	debug   bool
	listen  bool
	echo    bool
	address string
	port    int64
	wait    float64
	threads int
}
type statistics struct {
	all    int64
	ok     int
	failed int
}

var opt options
var wg sync.WaitGroup
var stats statistics

func udpServer() {
	buff := make([]byte, 1024)
	hn, _ := os.Hostname()
	address := fmt.Sprintf("%s:%d", opt.address, opt.port)
	fmt.Printf("Starting server on %s\n", address)

	conn, err := net.ListenPacket("udp", address)
	if err != nil {
		fmt.Printf("[Error] %s\n", err.Error())
	}
	for {
		n, addr, err := conn.ReadFrom(buff)
		if err != nil {
			fmt.Printf("[Error] %s\n", err.Error())
			break
		}

		answer := fmt.Sprintf("%s(%s) - From: %s - Msg: %s", time.Now().Format("2006-01-02 15:04:05"), hn, addr, string(buff[:n]))
		if opt.echo {
			conn.WriteTo([]byte(answer), addr)
		}

	}
	conn.Close()
}

func udpSendRcv(conn net.Conn, b []byte) string {
	buff := make([]byte, 1024)
	var err error
	_, err = conn.Write(b)

	if err != nil {
		println("Write data failed:", err.Error())
	}

	_, err = conn.Read(buff)
	if err != nil {
		println("Read data failed:", err.Error())
	}

	r, _ := regexp.Compile(`.*\((.*?)\).*`)
	res := r.FindSubmatch(buff)
	if len(res) > 1 {
		return string(res[1])
	} else {
		return ""
	}
}

func udpClient() {
	defer wg.Done()
	address := fmt.Sprintf("%s:%d", opt.address, opt.port)
	udpServer, err := net.ResolveUDPAddr("udp", address)

	if err != nil {
		println("ResolveUDPAddr failed:", err.Error())
		return
	}

	conn, err := net.DialUDP("udp", nil, udpServer)
	if err != nil {
		println("Listen failed:", err.Error())
		return
	}

	resp_org := string(udpSendRcv(conn, []byte("ping")))
	if resp_org == "" {
		stats.failed++
		time.Sleep(time.Duration(1000*opt.wait) * time.Millisecond)
	} else {
		time.Sleep(time.Duration(1000*opt.wait) * time.Millisecond)
		resp := string(udpSendRcv(conn, []byte("ping")))

		if resp_org == resp {
			stats.ok++
		} else {
			fmt.Printf("[NOT OK] %s - %s\n", resp_org, resp)
			stats.failed++
		}
	}
	conn.Close()

}

func main() {

	flag.BoolVar(&opt.debug, "d", false, "Debug")
	flag.BoolVar(&opt.listen, "l", false, "Listen mode")
	flag.BoolVar(&opt.echo, "e", false, "Echo mode")
	flag.Float64Var(&opt.wait, "w", 1.0, "Wait sec")
	flag.IntVar(&opt.threads, "t", 1, "Threads")
	flag.Int64Var(&opt.port, "p", 5060, "Port")
	flag.Parse()
	opt.address = "localhost"
	if flag.Arg(0) != "" {
		opt.address = flag.Arg(0)
	}
	if opt.listen {
		udpServer()
	} else {
		stats = statistics{all: 0, ok: 0, failed: 0}
		for {
			for i := 0; i < opt.threads; i++ {
				wg.Add(1)
				go udpClient()
			}
			wg.Wait()
			fmt.Printf("\rOk: %5d, Failed: %5d", stats.ok, stats.failed)
		}

	}
}
